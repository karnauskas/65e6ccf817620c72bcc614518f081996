FROM debian:buster

RUN apt-get update \
	&& apt-get install -y --no-install-recommends \
	bc \
	bison \
	build-essential \
	cpio \
	fakeroot \
	flex \
	kmod \
	libelf-dev \
	libncurses-dev \
	libncurses5-dev \
	libssl-dev \
	rsync \
	&& apt clean all

RUN useradd -m -d /home/build -u 1000 -s /bin/bash build

USER build